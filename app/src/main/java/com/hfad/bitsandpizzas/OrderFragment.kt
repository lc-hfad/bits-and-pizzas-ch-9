package com.hfad.bitsandpizzas

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.chip.Chip
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.switchmaterial.SwitchMaterial

class OrderFragment : Fragment() {
    override fun onCreateView(inflater:           LayoutInflater,
                              container:          ViewGroup?,
                              savedInstanceState: Bundle?)
        : View?
    {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_order, container, false)

        val toolbar = view.findViewById<MaterialToolbar>(R.id.toolbar)
        (activity as AppCompatActivity).setSupportActionBar(toolbar)

        val fab = view.findViewById<FloatingActionButton>(R.id.fab)

        fab.setOnClickListener {
            val pizzaGroup = view.findViewById<RadioGroup>(R.id.pizza_group)
            val pizzaType = pizzaGroup.checkedRadioButtonId

            if (pizzaType == -1) {
                val text = "You must select a pizza type."
                Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()

            } else {
                val parmesan  = view.findViewById<CheckBox>(R.id.parmesan)
                val pepperoni = view.findViewById<SwitchMaterial>(R.id.pepperoni)
                val cheesy    = view.findViewById<ToggleButton>(R.id.toggle_button)
                val pineapple = view.findViewById<Chip>(R.id.pineapple)
                val kiwi      = view.findViewById<Chip>(R.id.kiwi)
                val ketchup   = view.findViewById<Chip>(R.id.ketchup)

                var text = "Order confirmed: " +
                    when (pizzaType) {
                        R.id.radio_diavolo -> "Diavolo pizza"
                        R.id.radio_funghi  -> "Funghi pizza"
                        R.id.radio_pan     -> "Pan pizza"
                        else               -> "An unknown pizza! TODO: error?"
                    }

                text += if (parmesan.isChecked)  ", extra parmesan" else ""
                text += if (pepperoni.isChecked) ", pepperoni" else ""
                text += if (cheesy.isChecked)    ", cheese" else ", no cheese"
                text += if (pineapple.isChecked) ", pineapple" else ""
                text += if (kiwi.isChecked)      ", kiwi" else ""
                text += if (ketchup.isChecked)   ", ketchup as sauce" else ""

                Snackbar.make(fab, text, Snackbar.LENGTH_SHORT)
                    .show()
            }
        }

        return view
    }
}